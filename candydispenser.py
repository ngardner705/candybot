#!/usr/bin/python

from __future__ import division # python 2 cant math
from threading import Thread
import subprocess
import time
import RPi.GPIO as GPIO
import curses

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define BCM GPIO pins for pen and motors
spinnerPins = [17,18,27,22]
conveyorPins = [5,6,13,19]

# Setup curses - screen and keyboard input
stdscr = curses.initscr()
curses.noecho() # dont show user input
curses.cbreak() # read input as they type, not with enter

dispenseTime = (5/4)
conveyTime = 2

#
# Candy Dispenser
# Nathan Gardner <nathan@facotry8.com>
# COVID Halloween 2020
#
#################################################
#
# Motor1 and 2
# 1 Stepper motors with a standard interface
# 28BYJ-48 stepper motor
# ULN2003 chip driver
#
# ###############################################
#
# Pass in GPIO object, and pinConfig dictionary
# PINS MUST USE BCM GPIO NUMBERS!

class CandyDispenser():

    # Constructor
    def __init__(self,GPIO,spinnerPins,conveyorPins):
        self.LoopSpeed = 0.002
        self.SpinnerMotor = CandyMotor(GPIO,spinnerPins)
        self.ConveyorMotor = CandyMotor(GPIO,conveyorPins)
    
    # Call this every loop
    def main(self):

        # Step the motors as needed
        self.SpinnerMotor.step()
        self.ConveyorMotor.step()
        
        # Wait before moving on
        time.sleep(self.LoopSpeed)
    
    def dispenseCandy(self):

        spinnerThread = Thread(target=self.spinnerProcess)
        conveyorThread = Thread(target=self.converyorProcess)
        spinnerThread.start()
        conveyorThread.start()

    # start moving
    def startMoving(self):
        print("Started spinner\n")
        self.SpinnerMotor.setDirection(-1)

    # stop moving
    def stopMoving(self):
        print("Stopped spinner\n")
        self.SpinnerMotor.setDirection(0)
    
    # start moving
    def startConveyor(self):
        print("Started conveyor\n")
        self.ConveyorMotor.setDirection(1)

    # stop moving
    def stopConveyor(self):
        print("Stopped conveyor\n")
        self.ConveyorMotor.setDirection(0)

    def spinnerProcess(self):
        self.startMoving()
        time.sleep(dispenseTime)
        self.stopMoving()

    def converyorProcess(self):
        time.sleep(dispenseTime)
        self.startConveyor()
        time.sleep(conveyTime)
        self.stopConveyor()

    def adjustSpinner(self):
        time.sleep(0.1)
        self.stopMoving()

class CandyMotor(object):
    
    def __init__(self, GPIO, pins):
        # setup pins
        self.pins = pins
        for pin in self.pins:
            GPIO.setup(pin,GPIO.OUT)
            GPIO.output(pin, False)
        
        # motor sequence
        self.stepSequence = [[1,0,0,1],
            [1,1,0,0],
            [0,1,1,0],
            [0,0,1,1]]
        self.stepSequencePosition = 0
        self.stepCounter = 0
        
        # 1 = Clockwise, -1 = Counter Clockwise, 0 = Off
        self.direction = 0
        
        # Speed
        self.speed = 0.002
    
    def setDirection(self,direction):
        self.direction = direction
        
    def step(self):

        # do we have a direction to step
        if(self.direction != 0):
            
            #print("Spinning\n")

            # We have to set 4 pins to take a step
            for pinIndex in range(0, 4):
                
                # get the GPIO pin number
                pin = self.pins[pinIndex]
                if self.stepSequence[self.stepSequencePosition][pinIndex]!=0:
                    GPIO.output(pin, True)
                else:
                    GPIO.output(pin, False)
            
            # Count the step
            self.stepCounter += self.direction
            self.stepSequencePosition += self.direction;
            
            # If we reach the end of the sequence, start again
            if (self.stepSequencePosition >= len(self.stepSequence)):
                self.stepSequencePosition = 0
            if (self.stepSequencePosition <= -len(self.stepSequence)):
                self.stepSequencePosition = 0

# Candy Dispenser
dispenser = CandyDispenser(GPIO,spinnerPins,conveyorPins)

try:  
    # here you put your main loop or block of code
    if __name__ == "__main__":

        stdscr.nodelay(True);

        print("Started Candy Dispenser\n")
        print("Press space bar to dispense.\n\n")

        while True:
            # Get the users last input
            userInput = stdscr.getch()
            
            # Spacebar dispenses
            if userInput == ord(" "):
                dispenser.dispenseCandy()

            # + key adjusts spinner
            if userInput == ord("+"):
                dispenser.startMoving()
                myThread = Thread(target=dispenser.adjustSpinner)
                myThread.start()

            # unlimited candy
            if userInput == ord("1"):
                dispenser.startMoving()
                dispenser.startConveyor()

            # stop it
            if userInput == ord("0"):
                dispenser.stopMoving()
                dispenser.stopConveyor()
            
            # Let candy do its thing
            dispenser.main()

except KeyboardInterrupt:  
    # here you put any code you want to run before the program   
    # exits when you press CTRL+C  
    GPIO.cleanup()
    print("\nForce Closed Candy Dispenser") # print value of counter  
except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working 
    GPIO.cleanup()
    print("Candy Dispenser Crashed")  
finally:
    GPIO.cleanup()
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()
    print("Stopped Candy Dispenser")
    #GPIO.cleanup() # this ensures a clean exit 